# Pinouts of Broadlink RM devices

You can replace the CPU module with your own microprocessor (eg. ESP8266, RPi Zero, OPi Zero, etc...)

Some Notes:
* The main voltage used by the entire board is 3.3V
* There is only a small trace of 5V near the MicroUSB connector
* LEDs are active-low (i.e. output LOW to turn the LED on)

![alt text](RM2_V1_thumbnail.jpg)

More versions of the board will be added in the future.
You're welcome to add a pull request.

**Disclaimer:** Use at your own risk. This was done entirely by reverse-engineering effort. Originally.US is not associated with Broadlink in any way.